﻿using System.Linq;
using System.Reflection;

namespace excercicio1;

internal class Program{
    static void Main(string[] args){
        var linhas = File.ReadAllLines("NFe.csv").Select(linha => linha.Split(';')[4]);

        foreach (var linha in linhas)
        {
            Console.WriteLine(linha);
        }
        
    }
}