public class Aluno{
    private string Nome {get ; set;}
    private string Turma {get ; set;}

    private double Media {get; set;}

    public Aluno(string Nome, string Turma, double Media){
        this.Nome = Nome;
        this.Turma = Turma;
        this.Media = Media;
    }
    public string getTurma(){
        return this.Turma;
    }
    public string getNome(){
        return this.Nome;
    }
    public double getMedia(){
        return this.Media;
    }

}