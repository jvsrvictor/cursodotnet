﻿using System.Linq;
using System.Reflection;

namespace excercicio1;

internal class Program{
    static void Main(string[] args){
        
        var alunos = new List<Aluno>
        {
            new Aluno ("João", "A", 7.6),
            new Aluno ("Maria", "B", 10),
            new Aluno ("Pedro", "A", 6.7)
        };

        var alunosDaTurmaA = alunos.Where(aluno => aluno.getTurma() == "A");
        var alunosDaTurmaAquePassaram = alunosDaTurmaA.Where(aluno => aluno.getMedia() >= 7);

        foreach (var aluno in alunosDaTurmaAquePassaram)
        {
            Console.WriteLine(aluno.getNome());
        }
    }
}