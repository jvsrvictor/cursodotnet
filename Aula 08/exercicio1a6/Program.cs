﻿// Delegate Simples
Operation teste = Calculator.Subtract;
Calculator.ExecuteOperation(teste, 10 ,5);

// Delegate com Funções Anônimas
Calculator.ExecuteOperation(delegate (int x, int y) { return x / y; }, 10, 5);

// Delegate com Expressões Lambda
Calculator.ExecuteOperation((x, y) => x * y, 10, 5);

// Lambda e Coleções
List<string> nomes = new List<string> { "José", "Victor" };
nomes.ForEach(CadaNome => Console.WriteLine(CadaNome.ToUpper()));

// Lambdas e LINQ
List<int> numbers = new List<int> { 1, 2, 3, 4, 5, 6 };
var evenNumbers = numbers.Where(n => n % 2 == 0);

System.Timers.Timer timer = new System.Timers.Timer(1000);
timer.Elapsed += (sender, e) => Console.WriteLine($"Tick: {DateTime.Now}, sender: {sender} e: {e}");
timer.Start();

while(true){
    Thread.Sleep(1);
}