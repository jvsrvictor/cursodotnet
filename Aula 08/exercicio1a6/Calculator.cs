public delegate int Operation(int x, int y);

public class Calculator
{
    public static int Add(int x, int y) => x + y;
    public static int Subtract(int x, int y) => x - y;
    public static int Multiply(int x, int y) => x * y;
    public static int Divide(int x, int y) => x / y;

    public static void ExecuteOperation(Operation op, int x, int y)
    {
        int result = op(x, y);
        Console.WriteLine($"Resultado: {result}");
        
    }
}
