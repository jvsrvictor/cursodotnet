public class Circulo : FormaGeometrica
{
    public float Raio{ get; set;}

    public override void CalcularArea(){
        float Area = (float)(3.14 *Raio*Raio);
        Console.WriteLine("Área = " + Area);
    }

    public override void CalcularPerimetro(){
        float Perimetro = (float)(2 *3.14*Raio);
        Console.WriteLine("Perímetro = " + Perimetro);
    }

    public Circulo (float Raio){
        this.Raio = Raio;
    }
}