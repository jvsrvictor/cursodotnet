public class Retangulo : FormaGeometrica
{
    public float LadoA{ get; set;}
    public float LadoB{ get; set;}

    public override void CalcularArea(){
        float Area = LadoA*LadoB;
        Console.WriteLine("Área = " + Area);
    }

    public override void CalcularPerimetro(){
        float Perimetro = 2*LadoA + 2*LadoB;
        Console.WriteLine("Perímetro = " + Perimetro);
    }

    public Retangulo (float LadoA, float LadoB){
        this.LadoA = LadoA;
        this.LadoB = LadoB;
    }
}