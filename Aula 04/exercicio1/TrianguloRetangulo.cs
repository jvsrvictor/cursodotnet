public class TrianguloRetangulo : FormaGeometrica
{
    public float Base{ get; set;}
    public float Altura{ get; set;}

    public override void CalcularArea(){
        float Area = (float)(Base *Altura*0.5);
        Console.WriteLine("Área = " + Area);
    }

    public override void CalcularPerimetro(){
        float Perimetro = Base + Altura + (float)Math.Sqrt(Base*Base + Altura*Altura);
        Console.WriteLine("Perímetro = " + Perimetro);
    }

    public TrianguloRetangulo (float Base, float Altura){
        this.Altura = Altura;
        this.Base = Base;
    }
}