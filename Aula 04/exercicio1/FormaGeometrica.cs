public abstract class FormaGeometrica
{
    public abstract void CalcularArea();

    public abstract void CalcularPerimetro();
}