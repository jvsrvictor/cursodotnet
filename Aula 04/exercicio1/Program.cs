﻿// BARALHO UNO

using System.Reflection;

namespace excercicio3;

internal class Program{
    static void Main(string[] args){
        GenericList<FormaGeometrica> FormasGeometricas = new GenericList<FormaGeometrica>();

        Circulo Circ1 = new Circulo(10);
        Circ1.CalcularArea();
        Circ1.CalcularPerimetro();

        FormasGeometricas.Add(Circ1);

        Retangulo Ret1 = new Retangulo(10, 10);
        Ret1.CalcularArea();
        Ret1.CalcularPerimetro();

        FormasGeometricas.Add(Ret1);

        TrianguloRetangulo TRet1 = new TrianguloRetangulo(10, 10);
        TRet1.CalcularArea();
        TRet1.CalcularPerimetro();

        FormasGeometricas.Add(TRet1);

    }
}