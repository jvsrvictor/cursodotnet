public class RegistroProdutos : Registro<string>
{
    private List<string> Lista = new List<string>();

    public override void AdicionarItem(string Item)
    {
        Lista.Add(Item);
    }

    public override void ListarItens()
    {   
        Console.Write("\n");
        foreach(string Produto in Lista){
            Console.Write(" " + Produto);
        }
        Console.Write("\n");
    }

    public override void RemoverItem(string Item)
    {
        Lista.Remove(Item);
    }
}