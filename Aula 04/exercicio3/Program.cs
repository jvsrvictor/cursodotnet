﻿using System.Reflection;

namespace exercicio3;

internal class Program{
    static void Main(string[] args){
        RegistroClientes RegCli = new RegistroClientes();
        RegCli.AdicionarItem("José");
        RegCli.AdicionarItem("Victor");
        RegCli.ListarItens();
        RegCli.RemoverItem("José");
        RegCli.ListarItens();

        RegistroProdutos RegProd = new RegistroProdutos();
        RegProd.AdicionarItem("Sabão");
        RegProd.AdicionarItem("Detergente");
        RegProd.ListarItens();
        RegProd.RemoverItem("Detergente");
        RegProd.ListarItens();
    }
}