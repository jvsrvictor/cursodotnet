public abstract class Registro<T>{
    public abstract void AdicionarItem(T Item);
    public abstract void RemoverItem(T Item);
    public abstract void ListarItens ();
}