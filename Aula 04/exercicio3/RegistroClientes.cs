public class RegistroClientes : Registro<string>
{
    private List<string> Lista = new List<string>();

    public override void AdicionarItem(string Item)
    {
        Lista.Add(Item);
    }

    public override void ListarItens()
    {   
        Console.Write("\n");
        foreach(string Cliente in Lista){
            Console.Write(" " + Cliente);
        }
        Console.Write("\n");
    }

    public override void RemoverItem(string Item)
    {
        Lista.Remove(Item);
    }
}