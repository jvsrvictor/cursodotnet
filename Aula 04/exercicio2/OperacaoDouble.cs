public class OperacaoDouble : OperacaoMatematica<double>{
    public override void Somar(double TermoA, double TermoB){
        double resultado = TermoA + TermoB;
        Console.WriteLine(TermoA + " + " + TermoB + " = " + resultado);
    }

    public override void Subtrair(double TermoA, double TermoB)
    {
        double resultado = TermoA - TermoB;
        Console.WriteLine(TermoA + " - " + TermoB + " = " + resultado);
    }

    public override void Multiplicar(double TermoA, double TermoB)
    {
        double resultado = TermoA * TermoB;
        Console.WriteLine(TermoA + " x " + TermoB + " = " + resultado);
    }

    public override void Dividir(double TermoA, double TermoB)
    {
        double resultado = TermoA / TermoB;
        Console.WriteLine(TermoA + " / " + TermoB + " = " + resultado);
    }
}