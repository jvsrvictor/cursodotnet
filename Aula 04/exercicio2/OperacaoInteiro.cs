public class OperacaoInteiro : OperacaoMatematica<int>{
    public override void Somar(int TermoA, int TermoB){
        int resultado = TermoA + TermoB;
        Console.WriteLine(TermoA + " + " + TermoB + " = " + resultado);
    }

    public override void Subtrair(int TermoA, int TermoB)
    {
        int resultado = TermoA - TermoB;
        Console.WriteLine(TermoA + " - " + TermoB + " = " + resultado);
    }

    public override void Multiplicar(int TermoA, int TermoB)
    {
        int resultado = TermoA * TermoB;
        Console.WriteLine(TermoA + " x " + TermoB + " = " + resultado);
    }

    public override void Dividir(int TermoA, int TermoB)
    {
        int resultado = TermoA / TermoB;
        Console.WriteLine(TermoA + " / " + TermoB + " = " + resultado);
    }
}