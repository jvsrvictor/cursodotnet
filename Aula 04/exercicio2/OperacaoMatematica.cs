public abstract class OperacaoMatematica<T>{
    public abstract void Somar(T TermoA, T TermoB);
    public abstract void Subtrair(T TermoA, T TermoB);
    public abstract void Multiplicar (T TermoA, T TermoB);
    public abstract void Dividir(T TermoA, T TermoB);
}