﻿using System.Reflection;

namespace exercicio2;

internal class Program{
    static void Main(string[] args){
        OperacaoInteiro Op1 = new OperacaoInteiro();
        Op1.Somar(10,5);
        Op1.Subtrair(10,5);
        Op1.Multiplicar(10,5);
        Op1.Dividir(10,5);

        OperacaoDouble Op2 = new OperacaoDouble();
        Op2.Somar(10,5);
        Op2.Subtrair(10,5);
        Op2.Multiplicar(10,5);
        Op2.Dividir(10,5);


    }
}