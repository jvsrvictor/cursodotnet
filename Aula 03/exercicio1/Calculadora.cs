public class Calculadora{
    private float PrimeiroTermo {get ; set;}
    private float SegundoTermo {get ; set;}

    public void Adicao(){
        Console.WriteLine(PrimeiroTermo + SegundoTermo);
    }

    public void Subtracao(){
        Console.WriteLine(PrimeiroTermo - SegundoTermo);
    }

    public void Multiplicacao(){
        Console.WriteLine(PrimeiroTermo * SegundoTermo);
    }

    public void Divisao(){
        if(SegundoTermo == 0){
            Console.WriteLine("Erro: Divisão por Zero!");
        }else{
            Console.WriteLine(PrimeiroTermo / SegundoTermo);
        }
    }

    public void SetPrimeiroTermo(float PrimeiroTermo){
        this.PrimeiroTermo = PrimeiroTermo;
    }

    public void SetSegundoTermo(float SegundoTermo){
        this.SegundoTermo = SegundoTermo;
    }


}