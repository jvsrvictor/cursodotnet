﻿// BARALHO UNO

using System.Reflection;

namespace excercicio3;

internal class Program{
    static void Main(string[] args){
        // INICIA O BARALHO DE UNO
        Baralho.IniciaBaralho();
        
        // INSTANCIA OS JOGADORES AO PASSO QUE DISTRIBUI AS CARTAS
        Jogador Jogador1 = new Jogador(1, Baralho.GetCartasJogador(7));
        Jogador Jogador2 = new Jogador(2, Baralho.GetCartasJogador(7));
        Jogador Jogador3 = new Jogador(3, Baralho.GetCartasJogador(7));
        Jogador Jogador4 = new Jogador(4, Baralho.GetCartasJogador(7));
        
    }
}