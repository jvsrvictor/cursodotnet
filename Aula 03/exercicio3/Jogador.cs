public class Jogador
{
    private int Numero {get ; set;}
    private List<Carta> CartasDoJogador {get ; set;}

    public Jogador(int Numero, List<Carta> CartasDoJogador){
        this.Numero = Numero;
        this.CartasDoJogador = CartasDoJogador;

        Console.WriteLine("O JOGADOR DE NÚMERO " + Numero + " RECEBEU AS SEGUINTES CARTAS:");

        //IMPRIME CARTAS RECEBIDAS
        foreach(Carta Carta in CartasDoJogador){
            Carta.ImprimeCarta();
        }

        Console.WriteLine("\n");
    }


}