public static class Baralho
{
    private static Stack<Carta>? PilhaCartas {get ; set;}
    private static Random rng = new Random();  

    // MÉTODO PARA EMBARALHAR UM LISTA QUALQUER
    public static void Shuffle<T>(this IList<T> list)  
    {  
        int n = list.Count;  
        while (n > 1) {  
            n--;  
            int k = rng.Next(n + 1);  
            T value = list[k];  
            list[k] = list[n];  
            list[n] = value;  
        }  
    }

    // CONSTRUTOR DA CLASSE BARALHO
    public static void IniciaBaralho()
    {
        List<string> Cores = new List<string>
        {
            "VERMELHO", "AMARELO", "VERDE", "AZUL"
        };

        List<Carta> Deck = new List<Carta>();

        // 2x(1 - 9) | 0 | REVERSO | BLOQUEIA | +2
        foreach(string Cor in Cores){
            
            for(int i = 1 ; i < 10 ; i++){
                Deck.Add(new Carta(i.ToString(), Cor));
            }

            Deck.Add(new Carta("0", Cor));
            Deck.Add(new Carta("REVERSO", Cor));
            Deck.Add(new Carta("BLOQUEIA", Cor));
            Deck.Add(new Carta("+2", Cor));
        }

        // 4x(ESCOLHE COR | +4)
        for(int i = 0 ; i < 4 ; i++){
            Deck.Add(new Carta("ESCOLHE COR", "CORINGA"));
            Deck.Add(new Carta("+4", "CORINGA"));
        }

        Shuffle(Deck);
        PilhaCartas = new Stack<Carta>(Deck);

    }

    // MÉTODO PARA RETIRAR A PRIMEIRA CARTA DA PILHA DE CARTAS
    private static Carta RetiraDaPilha()
    {
        return PilhaCartas.Pop();
    }

    // MÉTODO PARA DISTRIBUIR AS CARTAS PARA OS JOGADORES
    public static List<Carta> GetCartasJogador(int numero)
    {   
        // CRIA UMA LISTA
        List<Carta> CartasJogador = new List<Carta>();

        // ADICIONA AS CARTAS DO DECK EMBARALHADO PARA A LISTA DO JOGADOR
        for(int i = 0 ; i < numero ; i++){
            CartasJogador.Add(RetiraDaPilha());
        }

        return CartasJogador;
    }


}