public class Carta
{
    private string Tipo {get ; set;}
    private string Cor {get ; set;}

    // CONTRUTOR DA CLASSE CARTA
    public Carta(string Tipo, string Cor){
        this.Tipo = Tipo;
        this.Cor = Cor;
    }

    public void ImprimeCarta(){
        Console.WriteLine(Tipo + " | " + Cor);
    }

}