public class Turma{
    private string Nome {get ; set;}
    private List<Aluno> Alunos = new List<Aluno>();

    public Turma(string Nome){
        this.Nome = Nome;
    }

    public void GetMedia(){
        float Media = 0;

        foreach(Aluno Aluno in Alunos){
            Media = Media + Aluno.GetNota()/Alunos.Count;
        }

        Console.WriteLine("A média da Turma de " + Nome + " é " + Media);
    }

    public void AddAluno(Aluno Aluno){
        Alunos.Add(Aluno);
    }

}