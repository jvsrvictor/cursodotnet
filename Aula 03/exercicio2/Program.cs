﻿using System.Reflection;

namespace excercicio2;

internal class Program{
    static void Main(string[] args){
        
        var Turma1 = new Turma("Computação");

        var Aluno1 = new Aluno("José", 10);
        Turma1.AddAluno(Aluno1);

        var Aluno2 = new Aluno("Victor", 5);
        Turma1.AddAluno(Aluno2);

        var Aluno3 = new Aluno("Silva", 10);
        Turma1.AddAluno(Aluno3);

        var Aluno4 = new Aluno("Rocha", 5);
        Turma1.AddAluno(Aluno4);

        var Aluno5 = new Aluno("João", 7);
        Turma1.AddAluno(Aluno5);

        Turma1.GetMedia();

    }
}