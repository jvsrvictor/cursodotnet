public class Aluno{
    private string Nome {get ; set;}
    private float NotaGeral {get ; set;}

    public Aluno(string Nome, float NotaGeral){
        this.Nome = Nome;
        this.NotaGeral = NotaGeral;
    }

    public float GetNota(){
        return NotaGeral;
    }



}