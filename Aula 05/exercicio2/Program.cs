﻿using System.Reflection;

namespace exercicio2;

internal class Program{
    static void Main(string[] args){
        var Usuario1 = new Usuario("Victor", "victor123", "65sd4g6sd54");
        var Usuario2 = new Usuario("Ana", "ana123", "g4df6g54d");
        var Usuario3 = new Usuario("Banana", "banana123", "s54dfg5s4");

        Autenticador.Autentica("victor123", "65sd4g6sd54");
        Autenticador.Autentica("ana123", "g4df6g54d");
        Autenticador.Autentica("banana123", "s54dfg5s4");

    }
}