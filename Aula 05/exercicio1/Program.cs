﻿using System.Reflection;

namespace exercicio1;

internal class Program{
    static void Main(string[] args){
        var Funcionario = new Funcionario("Victor", "Analista de Sistemas", 22200);

        var CalculadoraSalario = new CalculadoraSalario();
        Console.WriteLine("Salário calculado: R$ " + CalculadoraSalario.CalculaSalario(120, 1000));

        var EmailService = new EmailService();
        EmailService.EnviaEmail("Conteúdo do Email", "Victor");

        var BancoDeDados = new BancoDeDados();
        BancoDeDados.SalvaFuncionario("Victor", "Analista de Sistemas", 22200);

    }
}