﻿using System.Reflection;

namespace exercicio3;

internal class Program{
    static void Main(string[] args){
        Mensagem m1 = new Mensagem("Victor", "Eu", "oi");
        EnviadorDeSMS.EnviarSMS(m1);
        EnviadorDeEmail.EnviarEmail(m1);

        Mensagem m2 = new Mensagem("Victor", "inválido", "oi");
        EnviadorDeSMS.EnviarSMS(m2);
        EnviadorDeEmail.EnviarEmail(m2);
    }
}