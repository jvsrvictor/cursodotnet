public class Mensagem{
    private string Remetente {get ; set;}
    private string Destinatario {get ; set;}
    private string Conteudo {get ; set;}

    public Mensagem(string Remetente, string Destinatario, string Conteudo){
        this.Remetente = Remetente;
        this.Destinatario = Destinatario;
        this.Conteudo = Conteudo;

        Console.WriteLine("Nova mensagem criada!");
    }

    public string getDestinatario(){
        return Destinatario;
    }

}