﻿using System.Reflection;

namespace exercicio4;

internal class Program{
    static void Main(string[] args){
        Produto p1 = new Produto("Sabão", 5.50, 20);
        Produto p2 = new Produto("Pão", 10.00, 10);
        Produto p3 = new Produto("Ovo", 1.50, 50);

        AtualizadorDeEstoque.IncrementaEstoque(p1);
        AtualizadorDeEstoque.DecrementaEstoque(p2);

        RelatorioDeEstoque.GerarRelatorio();
    }
}