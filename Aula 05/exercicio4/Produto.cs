public class Produto{
    private string Nome {get ; set;}
    private double Preco {get ; set;}
    private int estoque {get ; set;}

    public Produto(string Nome, double Preco, int estoque){
        this.Nome = Nome;
        this.Preco = Preco;
        this.estoque = estoque;

        Console.WriteLine("Novo produto cadastrado!");
    }

    public void setEstoque(int estoque){
        this.estoque = estoque;
    }

    public int getEstoque(){
        return estoque;
    }

}