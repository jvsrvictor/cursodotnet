public static class AtualizadorDeEstoque{
    public static void IncrementaEstoque(Produto Produto){
        Produto.setEstoque(Produto.getEstoque() + 1);
        Console.WriteLine("Estoque atualizado!");
    }
    public static void DecrementaEstoque(Produto Produto){
        Produto.setEstoque(Produto.getEstoque() - 1);
        Console.WriteLine("Estoque atualizado!");
    }

}