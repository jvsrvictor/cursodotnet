programa
{
	inclua biblioteca Util --> util
	
	funcao inicio() 
	{
		inteiro pilha[5]
		inteiro soma = 0
		
		// Exibe a pilha na ordem original
		escreva ("A piha é:\n")
		para (inteiro posicao = 0; posicao < 5; posicao++)
		{
			pilha[posicao] = util.sorteia(1, 10) // Sorteia um número e atribui à posição da pilha
      escreva (pilha[posicao], " ")
      soma = soma + pilha[posicao]
    }

		
		escreva("\nA soma da pilha é: ", soma, "\n")
	}
}