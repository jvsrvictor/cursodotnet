programa
{
	inclua biblioteca Util --> util
	inclua biblioteca Texto --> tx


  funcao digito_mais_significativo (cadeia string){
    inteiro tamanho = (tx.numero_caracteres(string))
    caracter digito
    inteiro flag = 0

    para(inteiro posicao = 0; posicao < tamanho; posicao++){
      
      se(flag == 1){
        se(tx.obter_caracter(string, posicao) != '0'){
          digito = tx.obter_caracter(string, posicao)
          retorne digito
        }
      }
      
      se(tx.obter_caracter(string, posicao) == ','){
        flag = 1
      }
    }

	}

	funcao inicio() {
		cadeia numero

		escreva ("Digite um número:\n")
    leia(numero)
  
    escreva ("O dígito mais significativo é: ", digito_mais_significativo(numero))

	}
}

